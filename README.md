# Ansible playbook - Upgrade to DEV / RC / latest

## Prerequisites
- Install Ansible.
- Configure your SSH config file with the lab environment.
- Create a ansible_vars.yml file 

```shell
---
VERSION_NAME: luna
VERSION: "2023.09"
BUILD_MODE: dev
NUMERIC_VERSION: "2023.09.00"
DOCKER_PASSWORD: my_token
```

- and replace :
    - luna with the name of the LTS version you are using.
    - 2023.09 with the code of the LTS version you want to install.
    - my_token with your xivoxc Docker account password.
    - dev with :
        - dev for latestdev
        - rc for latestrc
        - leave it empty if you want latest

## Quick Command Example
To run the playbook, use the following command, replacing the necessary values:

- To upgrade the luna_dev lab :

```shell
ansible-playbook -i inventories/luna_lab upgrade.yml
```

Don't forget to replace:

- luna_lab with the lab of your choice (luna_lab, kuma_lab, jabbah_lab, or izar_lab).


## Variables
- VERSION: Major version number, e.g., 2023.09.
- VERSION_NAME: Major version name, e.g., luna.
- DOCKER_PASSWORD: The Docker password for the xivoxc account.

## Adding a Personal Environment
Create a new inventory file in the inventories folder with a descriptive name, e.g., luna_inventory.ini.

Populate the new inventory file with the appropriate group definitions and machine IP addresses specific to that environment. You can structure it similarly to the existing inventory files but tailored to your new environment.

You can now use your group in the command line for example :
```shell
ansible-playbook -i inventories/luna_inventory.ini -u root -e VERSION_NAME=luna -e VERSION=2023.09 -e DOCKER_PASSWORD=my_token upgrade_dev.yml --limit ese_terraform_tf
```

## Task / Roles Descriptions
all
- Change XIVOCC DIST to latestdev in custom.env.

XiVO && XiVO CC && Mds1
- Change the file /etc/apt/sources.list.d/xivo-dist.list to xivo-{VERSION_name}-dev.

XiVO
- Run the xivo-upgrade command.

XiVO CC
- Stop the XiVO CC Docker container with the xivocc-dcomp stop command.
- Update packages with apt update and apt dist-upgrade -y.
- Pull XiVO CC containers with xivocc-dcomp pull.
- Start XiVO CC containers with xivocc-dcomp up -d.

Mds1
- Run the mds-upgrade command.

Edge && Meetingroom
- Set XIVOCC_TAG to {VERSION} in .env.

Edge
- Stop Edge Docker containers with the docker-compose command.
- Update packages with apt update and apt dist-upgrade -y.
- Update Edge containers with docker-compose.
- Start Edge containers with docker-compose.

Meetingroom
- Log in to Docker with docker login -u xivoxc -p {DOCKER_PASSWORD}.
- Stop the Meetingroom Docker container with docker-compose.
- Update packages with apt update and apt dist-upgrade -y.
- Update Meetingroom containers with docker-compose.
- Start Meetingroom containers with docker-compose.
- Log out from Docker.

## Limitations
The upgrade of Edge and Meetingrooms is not correct because the YAML files are not updated.
Execution could be optimized for speed.
Variables are written in a rough manner and could be refined.
This update reflects the changes made in the new YAML playbook, providing more accurate task descriptions.